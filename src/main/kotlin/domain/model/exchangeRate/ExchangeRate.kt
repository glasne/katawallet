package domain.model.exchangeRate

import domain.model.financialAsset.FinancialAsset
import domain.model.wallet.Currency
import domain.model.wallet.Amount
import java.math.BigDecimal

data class ExchangeRate(val sourceType: FinancialAsset.Type, val targetCurrency: Currency, val rate: BigDecimal) {
    fun apply(financialAsset: FinancialAsset): Amount {
        return Amount(this.rate.multiply(financialAsset.quantity), this.targetCurrency)
    }
}