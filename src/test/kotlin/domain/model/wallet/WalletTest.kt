package domain.model.wallet

import domain.model.exchangeRate.ExchangeRate
import domain.model.financialAsset.FinancialAsset
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import java.math.BigDecimal
import kotlin.test.Test

class WalletTest {
    @Nested
    inner class WalletEvaluation {
        @Test
        fun `An empty Wallet should be valued to zero`() {
            val wallet = Wallet()
            val zeroEuro = Amount(BigDecimal.ZERO, Currency.EURO)

            wallet.evaluate(Currency.EURO, ExchangeRate(FinancialAsset.Type(Currency.EURO), Currency.EURO, BigDecimal.ONE)) shouldBe zeroEuro
        }

        @Test
        fun `A financial asset should be evaluated to its quantity and type`() {
            val financialAssetQuantity = BigDecimal.ONE
            val financialAssetTypeCurrency = Currency.EURO
            val financialAsset = FinancialAsset(financialAssetQuantity, FinancialAsset.Type(financialAssetTypeCurrency))
            val wallet = Wallet(financialAsset)
            val oneEuro = Amount(financialAssetQuantity, financialAssetTypeCurrency)

            wallet.evaluate(Currency.EURO, ExchangeRate(FinancialAsset.Type(Currency.EURO), Currency.EURO, BigDecimal.ONE)) shouldBe oneEuro
        }

        @Test
        fun `trois`() {
            val financialAsset = FinancialAsset(BigDecimal.ONE, FinancialAsset.Type(Currency.DOLLAR))
            val wallet = Wallet(financialAsset)
            val exchangeRate = ExchangeRate(FinancialAsset.Type(Currency.DOLLAR), Currency.EURO, BigDecimal(2))
            val twoEuro = Amount(BigDecimal(2), Currency.EURO)

            wallet.evaluate(Currency.EURO, exchangeRate) shouldBe twoEuro
        }
    }
}