package domain.model.wallet

import java.math.BigDecimal

data class Amount(
    val amount: BigDecimal,
    val currency: Currency
) {

}
