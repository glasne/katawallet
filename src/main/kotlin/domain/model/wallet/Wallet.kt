package domain.model.wallet

import domain.model.exchangeRate.ExchangeRate
import domain.model.financialAsset.FinancialAsset
import java.math.BigDecimal

class Wallet(private val financialAsset: FinancialAsset? = null) {
    fun evaluate(currency: Currency, exchangeRate: ExchangeRate): Amount {
        if (financialAsset != null) {
            return exchangeRate.apply(financialAsset)
        }
        return Amount(BigDecimal.ZERO, currency)
    }
}
