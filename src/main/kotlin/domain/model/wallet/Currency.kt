package domain.model.wallet

enum class Currency {
    EURO,
    DOLLAR
}