package domain.model.financialAsset

import domain.model.wallet.Currency
import java.math.BigDecimal

data class FinancialAsset(val quantity: BigDecimal, val type: Any) {
 data class Type(val value: Currency)
}
